var express = require('express');

var ac = require('atlassian-connect-express');

// Typical web stuff you'll need later
var http = require('http');
var path = require('path');

// Bootstrap Express
var app = express();

// Bootstrap the `atlassian-connect-express` library
var addon = ac(app);

// You can set this in `config.js`
var port = addon.config.port();

// Declares the environment to use in `config.js`
var devMode = app.get('env') == "development";

// The following settings applies to all environments.
app.set('port', port);

// Declare any Express [middleware](http://expressjs.com/api.html#middleware) you'd like to use here
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.cookieSession({
  key: 'session',
  secret: addon.config.secret() // Add your super secret salt in `config.js`
}));
// You need to instantiate the `atlassian-connect-express` middleware in order to get it's goodness for free
app.use(addon.middleware());
// This is where the routers are mounted
app.use(app.router);

// development only
if (devMode) {
  // Show nicer errors when in devMode
  app.use(express.errorHandler());
}

// Where all my webhook handlers are
require('./webhooks')(app, addon);

// Boot the damn thing
http.createServer(app).listen(port, function(){
  console.log('Add-on server running at http://localhost:' + port);
  if (devMode) {
    // Enables auto registration/de-registration of add-ons into a host
    addon.register();
  }
});