var colors = require('colors');
var Firebase = require('firebase');
var dataRef = new Firebase('https://connect.firebaseio.com/webhook-inspector');

module.exports = function (app, addon) {

    // Atlassian Connect Express fires events when a webhook is received, in order to authenticate the webhook for you.

    addon.descriptor.modules.webhooks.forEach(function (webhook) {
        addon.on(webhook.event, function(evt, body, req){
            var d = new Date;
            dataRef.child(req.session.clientKey).child(evt).child(d.toISOString().replace(".","_")).set(body);
            console.log();
            console.log(evt.bold.yellow.inverse);
            console.log(require('util').inspect(body, {
                colors: true,
                depth: 6,
                showHidden: true,
            }));
        });
    });
};
